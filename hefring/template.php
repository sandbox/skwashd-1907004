<?php

/**
 * Implements template_preprocess_page().
 */
function hefring_preprocess_page(&$vars) {
  $page =& $vars['page'];
  $vars['page_classes'] = '';

  if (!empty($vars['tabs2']['#secondary'])) {
    array_unshift($page['sidebar_first'], $vars['tabs2']);
    unset($vars['tabs2']);
  }
  if (empty($page['sidebar_first']) && empty($page['sidebar_second'])) {
    $vars['page_classes'] = 'wide';
  }
}
